# 3rd place solution

# Challenge Intermarché June 2021
Prediction of sales volumes of consumer goods.

The goal  is to provide a model for estimating daily sales volumes, made by different outlets for various consumer products (excluding fresh or variable weight products).

The model must be able to predict these volumes over a horizon of 3 months at the mesh day. Candidates must provide models that can be industrialized at scale and can be generalized to other products and points of sale.

Candidates have a learning base containing daily sales, a selection of products, made by a sample of points of sale over the year 2018. The algorithms will be assessed in relation to the volumes produced in the first quarter of 2019 .

More details here https://challenge.datafactory-intermarche.fr/fr/challenge/1/details

# How to run the code

`main.py` is the single entry point for the project.
data must be placed in the directory `data`

Create a clean venv then
```
pip install -r requirements.txt
python main.py
```

A Jupyter notebook `solution.ipynb` is also available to help understanding the solution.

# Linting/Formatting
Flake8, Black, isort

# License
This project is free and open-source software licensed under the MIT license.