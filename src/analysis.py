"""Analysis code. Not used in production.
"""
import pandas as pd
import plotnine as pn
from plotnine import aes, ggplot

from src import preprocessing as prep

sales = prep.read_sales()

agg_func = {"qte": "sum", "id_artc": "nunique"}

sales_agg = sales.groupby("date").agg(agg_func).reset_index()
sales_agg = sales_agg.rename(columns={"id_artc": "active_products"})
sales_agg[["qte_7", "active_products_7"]] = (
    sales_agg[["qte", "active_products"]].rolling(7).mean()
)
(ggplot(sales_agg, aes("date", "qte")) + pn.geoms.geom_line())
(
    ggplot(sales_agg, aes("date", "qte_7"))
    + pn.geoms.geom_line()
    + pn.scales.scale_y_continuous(limits=(0, pd.NA))
    + pn.labs(title="Is there a yearly pattern?")
)
(
    ggplot(sales_agg, aes("date", "active_products_7"))
    + pn.geoms.geom_line()
    + pn.scales.scale_y_continuous(limits=(0, pd.NA))
    + pn.labs(
        title="There is a survival bias. Products samples are the ones who had at least one sale during Q1 2019",  # noqa
        y="Weekly active products",
    )
)


nomenclature = prep.read_nomenclature()

nomenclature_sales = pd.merge(sales, nomenclature)
nomenclature_sales["date_dow"] = nomenclature_sales["date"].dt.dayofweek


col_category = "lb_vent_rayn"

categories_dow = (
    nomenclature_sales.groupby([col_category] + ["date_dow"])["qte"].sum().reset_index()
)
categories_dow["qte_mean"] = categories_dow["qte"] / categories_dow.groupby(
    col_category
)["qte"].transform("mean")

(
    ggplot(categories_dow, aes("date_dow", "qte_mean", color="lb_vent_rayn"))
    + pn.geom_line()
)

categories_agg = (
    nomenclature_sales.groupby(["date", "lb_vent_rayn"], observed=True)["qte"]
    .sum()
    .reset_index()
)
categories_agg = categories_agg[categories_agg["date"] != "2018-01-01"]

categories_agg["qte_7"] = categories_agg.groupby("lb_vent_rayn")["qte"].transform(
    lambda x: x.rolling(7).mean()
)

categories_agg["qte_7_normed"] = categories_agg["qte_7"] / categories_agg.groupby(
    "lb_vent_rayn", observed=True
)["qte_7"].transform("mean")

# categories_agg["qte_7_normed"] = categories_agg["qte_7_normed"].clip(0.75, 1.25)

categories_agg["text"] = (
    categories_agg.groupby("lb_vent_rayn", observed=True)["qte_7"]
    .transform("sum")
    .astype(int)
    .astype(str)
    + " "
    + categories_agg["lb_vent_rayn"].astype(str)
)

head = categories_agg.groupby("lb_vent_rayn").head(90).copy()

head["date"] = pd.to_datetime(head["date"].astype(str).str.replace("2018", "2019"))

head = pd.concat([categories_agg, head])

(
    ggplot(
        head.groupby("lb_vent_rayn").tail(150),
        aes("date", "qte_7_normed"),
    )
    + pn.geoms.geom_line()
    + pn.facet_grid("lb_vent_rayn ~ .", scales="free")
    + pn.theme(
        figure_size=(12, 9 * 2),
        strip_text_y=pn.element_text(
            angle=0, ha="left"  # change facet text angle  and alignment
        ),
    )
)

categories_agg


head[head["lb_vent_rayn"] == "CREMERIE LS"].tail(110).head(10)
