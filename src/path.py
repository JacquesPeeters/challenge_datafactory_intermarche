import os

from src import constants

DIR_ENV = f"data/{constants.CURRENT_ENVIRONMENT}"
os.makedirs(DIR_ENV, exist_ok=True)

path_learning = os.path.join(DIR_ENV, "learning.parquet")


def path_prod(date_origin):
    return os.path.join(DIR_ENV, f"prod_{date_origin}.parquet")


path_artefacts = os.path.join(DIR_ENV, "artefacts.pickle")


def path_submission(date_origin):
    return os.path.join(DIR_ENV, f"submission_{date_origin}.csv.zip")


def path_submission_raw(date_origin):
    return os.path.join(DIR_ENV, f"submission_raw_{date_origin}.parquet")
