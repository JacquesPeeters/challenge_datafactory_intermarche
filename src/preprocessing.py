import datetime

import holidays
import numpy as np
import pandas as pd

from src import constants
from src.utils import timer


def read_sales():
    sales = pd.read_csv("data/ventes_2018.csv")
    sales.columns = sales.columns.str.lower()
    sales["date"] = pd.to_datetime(sales["date"])
    sales["qte_lop1p"] = np.log1p(sales["qte"])  # Our target
    return sales


def sample_dataset(dataset):
    """Sample dataset at ["id_artc", "id_pdv"] granularities to speed up developement.
    Sampling factor depends on the CURRENT_ENVIRONMENT.
    Do it after the feature engineering at high granularities (fast to compute) and
    before the ones at low granularities (slow to compute).
    """
    if constants.SAMPLING_FACTOR != 0:
        print(f"Sample by a factor of {constants.SAMPLING_FACTOR}")
        dataset = dataset[
            (dataset["id_artc"] + dataset["id_pdv"]).mod(constants.SAMPLING_FACTOR) == 0
        ]
    return dataset


def read_prix() -> pd.DataFrame:
    """Read and clean prix dataset"""
    prix = pd.read_csv("data/prix_vente.csv")
    prix.columns = prix.columns.str.lower()

    prix = sample_dataset(prix)

    prix["prix_unitaire"] = prix["prix_unitaire"].str.replace(
        "Moins de 0.99€", "Entre 0 et 0.99€"
    )

    prix["prix"] = prix["prix_unitaire"].str.findall(r"\d+(?:\.\d+)?")

    prix["prix"] = (
        prix["prix"]
        .apply(lambda x: [float(i) for i in x])
        .apply(np.array)
        .apply(lambda x: x.mean())
    )

    def format_date(annee, trimestre):
        return datetime.datetime(annee, trimestre * 3 - 3 + 1, 1)

    format_date = np.vectorize(format_date)

    prix["date"] = format_date(prix["annee"], prix["trimestre"])
    prix = prix.drop(columns=["annee", "trimestre", "prix_unitaire"])

    prix = prix.sort_values("date")
    prix = prix.rename(columns={"date": "date_destination"})
    return prix


def read_nomenclature() -> pd.DataFrame:
    nomenclature = pd.read_csv("data/nomenclature_produits.csv")
    nomenclature.columns = nomenclature.columns.str.lower()
    cols = ["lb_vent_rayn", "lb_vent_faml", "lb_vent_sous_faml"]
    for col in cols:
        nomenclature[col] = pd.Categorical(nomenclature[col])
    return nomenclature


def read_points_de_vente() -> pd.DataFrame:
    # Not used in the solution
    points_de_vente = pd.read_csv("data/points_de_vente.csv")
    points_de_vente.columns = points_de_vente.columns.str.lower()
    points_de_vente["id_voct"] = points_de_vente["id_voct"].str[-1].astype(int)
    points_de_vente = points_de_vente[["id_pdv", "id_voct"]]
    return points_de_vente


def get_holidays_fr() -> pd.DataFrame:
    """For a given date_destination, return distance to next/previous holiday cliped
    between [-1, 1]

    Returns:
        pd.DataFrame: holidays_fr
    """
    holidays_fr = pd.DataFrame(
        pd.date_range(start="2018-01-01", end="2019-04-01"),
        columns=["date_destination"],
    )
    holidays_fr["is_holiday"] = holidays_fr["date_destination"].apply(
        lambda x: x in holidays.CountryHoliday("FRA")
    )

    tmp = np.where(holidays_fr["is_holiday"], holidays_fr["date_destination"], pd.NaT)
    holidays_fr["next_holiday"] = pd.to_datetime(pd.Series(tmp).bfill(limit=1))
    holidays_fr["previous_holiday"] = pd.to_datetime(pd.Series(tmp).ffill(limit=1))
    holidays_fr["previous_holiday_time"] = (
        holidays_fr["date_destination"] - holidays_fr["previous_holiday"]
    ).dt.days
    holidays_fr["next_holiday_time"] = (
        holidays_fr["date_destination"] - holidays_fr["next_holiday"]
    ).dt.days
    holidays_fr["is_holiday"] = holidays_fr[
        ["previous_holiday_time", "next_holiday_time"]
    ].ffill(axis=1)["next_holiday_time"]
    holidays_fr = holidays_fr[["date_destination", "is_holiday"]]
    return holidays_fr


def generate_products_date(sales: pd.DataFrame, start: str, end: str) -> pd.DataFrame:
    """Generate one line for each dates*["id_pdv", "id_artc"] between start and end date

    Args:
        sales (pd.DataFrame): [description]
        start (str): start_date (min)
        end (str): end_date (max)

    Returns:
        pd.DataFrame: [description]
    """
    dates_range = pd.date_range(start, end)
    dates_range = pd.DataFrame({"date": dates_range})
    # Find pairs ["id_pdv", "id_artc"] having at least one sale
    # here we consider that if an artc never been sold in id_pdv it cannot be sold in
    # the future
    products = sales[["id_pdv", "id_artc"]].drop_duplicates()
    # Cross join
    products_date = pd.merge(
        dates_range.eval("key=1"),
        products.eval("key=1"),
    ).drop(columns="key")
    return products_date


def generate_main(
    sales: pd.DataFrame, start: str, end: str, is_train: bool
) -> pd.DataFrame:
    """Generate the main dataset. Main dataset is the dataset at the granularity of our
    predictions ['id_pdv', 'id_artc', 'date_origin', 'horizon', 'date_destination']

    Args:
        sales (pd.DataFrame): sales dataset
        start (str): start date
        end (str): end date
        is_train (bool): is it for training (or prod)

    Returns:
        pd.DataFrame: main dataset
    """
    products_date_learning = generate_products_date(sales, start, end)

    if is_train:
        # We need to sample otherwise too big - a int would be better than an frac...
        products_date_learning = products_date_learning.sample(frac=0.1, random_state=1)

    horizon = pd.DataFrame({"horizon": range(1, constants.HORIZON_MAX)})
    main_learning = pd.merge(
        products_date_learning.eval("key=1"),
        horizon.eval("key=1"),
    ).drop(columns="key")
    main_learning = main_learning.rename(columns={"date": "date_origin"})
    main_learning["date_destination"] = main_learning["date_origin"] + pd.to_timedelta(
        main_learning["horizon"], unit="D"
    )
    return main_learning


def generate_main_prod(sales: pd.DataFrame, date_origin) -> pd.DataFrame:
    """Generate the main dataset for production. Main dataset is the dataset at the
    granularity of our predictions
    ['id_pdv', 'id_artc', 'date_origin', 'horizon', 'date_destination']

    Args:
        sales (pd.DataFrame): [description]
        date_origin ([type]): [description]

    Returns:
        pd.DataFrame: main_prod
    """
    main_prod = generate_main(
        sales,
        start=str(date_origin),
        end=str(date_origin),
        is_train=False,
    )
    # Since we are predicting for multiple date_origin, add this filter to avoid
    # predicting useless dates
    main_prod = main_prod[main_prod["date_destination"] >= "2019-01-01"]
    main_prod = main_prod[main_prod["date_destination"] <= "2019-03-31"]
    return main_prod


@timer
def generate_main_learning(sales: pd.DataFrame) -> pd.DataFrame:
    """Generate the main dataset. Main dataset is the dataset at the granularity of our
    predictions ['id_pdv', 'id_artc', 'date_origin', 'horizon', 'date_destination']
    and their associated target/sales.

    * We subjectively decide december is not a representative month and remove it
    (multiple stores exceptionnaly opens on sunday).

    * "2018-05-01" is a buggy date therefore is removed.
    This two magic tricks gave a huge boost ~0.01

    Also performs sample with constants.LEARNING_SIZE_MAX

    Args:
        sales (pd.DataFrame): sales data

    Returns:
        pd.DataFrame: main dataset for learning set
    """
    main_learning = generate_main(
        sales, start="2018-01-31", end="2018-12-01", is_train=True
    )

    # We know we don't have target < "2019-01-01"
    main_learning = main_learning[main_learning["date_destination"] < "2019-01-01"]

    main_learning = main_learning[main_learning["date_destination"] < "2018-12-01"]
    main_learning = main_learning[main_learning["date_destination"] != "2018-05-01"]

    # Add the target
    main_learning = pd.merge(
        main_learning,
        sales.rename(columns={"date": "date_destination"}),
        how="inner",
    )

    if main_learning.shape[0] > constants.LEARNING_SIZE_MAX:
        main_learning = main_learning.sample(
            constants.LEARNING_SIZE_MAX, random_state=1
        )

    return main_learning


def complete_sales(sales: pd.DataFrame) -> pd.DataFrame:
    """Fill implicit 0 values by generating one line for each day*product between start
    and end date and sales.

    "2018-05-01" is a buggy dates. Fill it with NA to avoid considering NA as 0 inside
    feature engineering.

    Args:
        sales (pd.DataFrame): sales data with only non negative sales

    Returns:
        pd.DataFrame: sales data with implicit 0 values
    """
    products_date = generate_products_date(sales, "2018-01-01", "2018-12-31")
    sales = pd.merge(products_date, sales, how="left")
    sales["qte"] = sales["qte"].fillna(0)
    sales["qte_lop1p"] = sales["qte_lop1p"].fillna(0)

    # Filter dates when products was not for sell yet
    cols_group = ["id_pdv", "id_artc"]
    sales_dates = (
        sales[sales["qte"] > 0].groupby(cols_group).agg({"date": ["min", "max"]})
    )
    sales_dates.columns = ["_".join(col) for col in sales_dates.columns]
    sales_dates = sales_dates.reset_index()

    # sales_dates['date_min'].max()
    # >> ''2018-07-03'
    # Each products has at least 6 months of training data - see later what we do with
    # this insight
    # Same some products are not for sale anymore

    sales = sales.merge(sales_dates[cols_group + ["date_min"]], how="left")

    sales = sales[sales["date"] >= sales["date_min"]]
    sales = sales.drop(columns="date_min")

    # Buggy date, do not use it inside FE
    cond = sales["date"] == "2018-05-01"
    sales.loc[cond, "qte"] = np.nan

    return sales
