import datetime
import os

import pandas as pd

CURRENT_ENVIRONMENT = os.getenv("CURRENT_ENVIRONMENT", "prod")
assert CURRENT_ENVIRONMENT in ("demo", "preprod", "prod")

# Predict from 2018-12-22 instead of 28-12-31 gives a 0.02 boost 0.573 to 0.571
DATE_ORIGIN_PROD = datetime.date(2018, 12, 10)
DATE_ORIGIN_PROD_END = datetime.date(2018, 12, 31)
RANGE_DATE_ORIGIN_PROD = pd.date_range(DATE_ORIGIN_PROD, DATE_ORIGIN_PROD_END)
RANGE_DATE_ORIGIN_PROD = [d.date() for d in RANGE_DATE_ORIGIN_PROD]
DATE_DESTINATION_PROD_MAX = datetime.date(2019, 3, 31)
HORIZON_MAX = (DATE_DESTINATION_PROD_MAX - DATE_ORIGIN_PROD) / datetime.timedelta(
    days=1
) + 1

# Max horizon in days of our forecats.
HORIZON_MAX = int(HORIZON_MAX)

# How much we want to sample datasets our pipeline to dev quickly
SAMPLING_FACTOR = {"demo": 100, "preprod": 10, "prod": 0}
SAMPLING_FACTOR = SAMPLING_FACTOR[CURRENT_ENVIRONMENT]

# Maximum size (number of rows) of learning set
LEARNING_SIZE_MAX = {
    # Going from 30M to 50M increase local score but worsen leaderboard.
    # We don't have a robust local validation
    "prod": 50_000_000,
    "preprod": 5_000_000,
    "demo": 5_000_000,
}
LEARNING_SIZE_MAX = LEARNING_SIZE_MAX[CURRENT_ENVIRONMENT]

IS_PARALLEL = True  # If we use parallel processing
