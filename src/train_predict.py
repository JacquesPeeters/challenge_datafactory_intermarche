import datetime
import pickle

import ipdb  # noqa - fix relative imports problems
import lightgbm as lgb
import mlflow

# How to make port redirection from remote to local to access MLflow UI
# ssh -N -f -L localhost:5000:localhost:5000 myusername@remotehost
import numpy as np
import pandas as pd
import plotnine as pn
from plotnine import aes, ggplot
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split

from src import constants, ml_utils, path, utils


def metric_mean_squared_log_error(df):
    # mean_squared_error on log values is equal to mean_squared_log_error
    return mean_squared_error(df["qte_lop1p"], df["pred"], squared=False)


def metric_mean_absolute_percentage_error(df):
    # APE = Absolute Percentage Error => check if we over/under predict specific day
    return (df["pred"].sum() / df["qte_lop1p"].sum()) / df["qte_lop1p"].sum()


class Model_LGB_Bagging:
    def __init__(self, l_params: list):
        """[Small class to easily bag multiple LGBM

        Args:
            l_params (list): list of dict. Each dict contains a set of params to feed a
            LGBM
        """
        self.l_params = l_params

    def _train_single(self, param, lgb_train, lgb_valid):
        # Train a single LGBM
        if lgb_valid is None:
            valid_sets = [lgb_train]
        else:
            valid_sets = [lgb_train, lgb_valid]

        model_lgb = lgb.train(
            params=param,
            train_set=lgb_train,
            num_boost_round=3000,
            valid_sets=valid_sets,
            early_stopping_rounds=300,
            verbose_eval=50,
        )
        return model_lgb

    def train(self, lgb_train, lgb_valid):
        model_lgb = []
        for param in self.l_params:
            model_lgb_single = self._train_single(param, lgb_train, lgb_valid)
            model_lgb.append(model_lgb_single)
        self.model_lgb = model_lgb
        return self.model_lgb

    def predict(self, X):
        preds = []
        for model_lgb_single in self.model_lgb:
            preds.append(model_lgb_single.predict(X))

        preds = np.mean(preds, axis=0)
        return preds

    def update_params(self):
        """Add num_boost_round params with model.best_iteration to retrain on full
        data
        """
        for param, model in zip(self.l_params, self.model_lgb):
            param["num_boost_round"] = model.best_iteration


def train() -> None:
    """Train the algorithm and dump its artefacts. Also logs various metrics/plots to
    MLFlow"""
    learning = pd.read_parquet(path.path_learning)
    learning = learning[learning["evaluated"]]  # See evaluated doc why we do this

    MIN_WEEK_HISTORY = 0
    learning = learning[
        learning["ID_PDV_&_ID_ARTC_date_origin_MINUS_date_min"] > 7 * MIN_WEEK_HISTORY
    ]

    # Avoid random train_test split due to sample of the line/mergeasof done before
    learning = learning.sort_values("date_destination")
    train, valid = train_test_split(learning, test_size=0.2, shuffle=False)

    target = "qte_lop1p"
    to_drop = [
        "date_origin",
        "date_destination",
        "id_pdv",
        "id_artc",
        "pred",
        "qte",
        "ID_PDV_&_ID_ARTC_date_min",
        "evaluated",
        "lb_vent_rayn",
        "lb_vent_faml",
        "lb_vent_sous_faml",
        "date_origin_week",
        "date_destination_week",
    ]
    X_cols = list(learning.drop(columns=to_drop + [target], errors="ignore"))

    X_learning = learning[X_cols]
    X_train = train[X_cols]
    X_valid = valid[X_cols]

    y_learning = learning[target]
    y_train = train[target]
    y_valid = valid[target]

    lgb_train = lgb.Dataset(X_train, label=y_train)
    lgb_valid = lgb.Dataset(X_valid, label=y_valid)
    lgb_learning = lgb.Dataset(X_learning, label=y_learning)

    l_params = []
    for num_leaves in [31, 63, 127]:
        # num_leaves is the main parameter to control the complexity of the tree model
        # bag 3 times the same model with different params. Helps to stabilize
        # predictions.
        param_tmp = {
            "objective": "regression_l2",
            "metric": "rmse",
            "learning_rate": 0.1,
            "random_state": 1,
            "verbosity": -1,
            "num_leaves": num_leaves,
        }
        l_params.append(param_tmp)

    model_lgb = Model_LGB_Bagging(l_params=l_params)
    model_lgb.train(lgb_train, lgb_valid)

    train["pred"] = model_lgb.predict(X_train)
    valid["pred"] = model_lgb.predict(X_valid)

    loss_horizon = (
        valid.groupby("horizon")
        .apply(metric_mean_squared_log_error)
        .rename("loss")
        .reset_index()
    )
    loss_horizon = loss_horizon.sort_values("horizon")
    loss_horizon["loss_90"] = (
        loss_horizon["loss"].rolling(90, min_periods=90).mean().shift(-90)
    )
    loss_horizon = loss_horizon[loss_horizon["loss_90"].notnull()]

    loss_horizon = (
        ggplot(loss_horizon, aes("horizon", "loss_90"))
        + pn.geoms.geom_line()
        + pn.labs(
            title="Is using a bigger horizon origin problematic?",
            x="Horizon origin",
            y="Rolling 90 mean_squared_log_error",
        )
        + pn.theme(axis_text_x=pn.element_text(rotation=45, hjust=1))
    )

    loss_horizon.save("data/loss_horizon.png")

    loss_date_destination = pd.concat(
        [
            train.groupby("date_destination")
            .apply(metric_mean_squared_log_error)
            .rename("mean_squared_log_error")
            .reset_index(),
            valid.groupby("date_destination")
            .apply(metric_mean_squared_log_error)
            .rename("mean_squared_log_error")
            .reset_index(),
        ]
    )

    loss_date_destination = (
        ggplot(loss_date_destination, aes("date_destination", "mean_squared_log_error"))
        + pn.geoms.geom_line()
        + pn.theme(axis_text_x=pn.element_text(rotation=45, hjust=1))
    )
    loss_date_destination.save("data/loss_date_destination.png")

    loss_date_destination = pd.concat(
        [
            train.groupby("date_destination")
            .apply(metric_mean_absolute_percentage_error)
            .rename("mean_absolute_percentage_error")
            .reset_index(),
            valid.groupby("date_destination")
            .apply(metric_mean_absolute_percentage_error)
            .rename("mean_absolute_percentage_error")
            .reset_index(),
        ]
    )

    loss_date_destination["mean_absolute_percentage_error"] = loss_date_destination[
        "mean_absolute_percentage_error"
    ].clip(0, 0.01)

    loss_date_destination = (
        ggplot(
            loss_date_destination,
            aes("date_destination", "mean_absolute_percentage_error"),
        )
        + pn.geoms.geom_line()
        + pn.theme(axis_text_x=pn.element_text(rotation=45, hjust=1))
    )
    loss_date_destination.save("data/loss_date_destination_ape.png")

    train_loss = mean_squared_error(y_train, train["pred"], squared=False)
    valid_loss = mean_squared_error(y_valid, valid["pred"], squared=False)

    importance = ml_utils.get_importance_lgb(model_lgb.model_lgb[0])
    plot_importance = ml_utils.plot_importance_lgb(importance.head(10))
    print(plot_importance)
    plot_importance.save("data/importance.png")
    importance.to_csv("data/importance.csv")

    mlflow.set_experiment("train")
    with mlflow.start_run():
        mlflow.log_param("ENV", constants.CURRENT_ENVIRONMENT)
        mlflow.log_metric("train_loss", train_loss)
        mlflow.log_metric("valid_loss", valid_loss)
        # mlflow.log_metric("best_iteration", model_lgb.best_iteration)
        mlflow.log_artifact("data/importance.png")
        mlflow.log_artifact("data/importance.csv")
        mlflow.log_artifact("data/loss_horizon.png")
        mlflow.log_artifact("data/loss_date_destination.png")
        mlflow.log_artifact("data/loss_date_destination_ape.png")
    mlflow.end_run()

    if constants.CURRENT_ENVIRONMENT == "prod":
        # Retrain on full data - always useful on timeseries
        model_lgb.update_params()
        l_params = model_lgb.l_params
        model_lgb = Model_LGB_Bagging(l_params=l_params)
        model_lgb.train(lgb_learning, None)

    # Dump artefacts
    artefacts = (model_lgb, X_cols)
    pickle.dump(artefacts, open(path.path_artefacts, "wb"))


def predict():
    """Predict prod dataset and create submission file for each date_origin"""
    (model_lgb, X_cols) = pickle.load(open(path.path_artefacts, "rb"))

    for date_origin in constants.RANGE_DATE_ORIGIN_PROD:
        # takes ~5mins for each run. Surprised that it is so slow
        prod = pd.read_parquet(path.path_prod(date_origin))
        X_prod = prod[X_cols]
        prod["pred"] = model_lgb.predict(X_prod)
        prod["pred"] = prod["pred"].clip(0)  # Sometimes lgb predict < 0

        prod.loc[prod["date_destination"] == "2019-01-01", "pred"] = 0

        submission = prod[["id_pdv", "id_artc", "date_destination", "pred"]].rename(
            columns={"pred": "qte"}
        )

        submission.to_parquet(path.path_submission_raw(date_origin))

        submission["qte"] = np.expm1(submission["qte"])  # reverse the log1p
        submission["qte"] = ml_utils.smart_round_to_int(submission["qte"])
        submission = submission[submission["qte"] != 0]
        submission["id"] = utils.format_id(
            submission["id_pdv"],
            submission["id_artc"],
            submission["date_destination"].dt.strftime("%Y%m%d"),
        )
        submission = submission[["id", "qte"]]
        submission.to_csv(
            path.path_submission(date_origin), compression="zip", index=False
        )


def bag_sub_date_origin():
    """Bag (average) predictions from different date_origin.
    Provides a small boost.
    """
    subs = []
    # The dates to bag.
    date_origin_bagg = [datetime.date(2018, 12, 20), datetime.date(2018, 12, 21)]
    for date_origin in date_origin_bagg:
        tmp = pd.read_parquet(path.path_submission_raw(date_origin))
        tmp["date_origin"] = date_origin
        subs.append(tmp)

    subs = pd.concat(subs)

    subs_bagg = subs[subs["date_origin"].isin(date_origin_bagg)]

    subs_bagg = (
        subs_bagg.groupby(["id_pdv", "id_artc", "date_destination"])["qte"]
        .mean()
        .reset_index()
    )

    subs_bagg["qte"] = np.expm1(subs_bagg["qte"])
    subs_bagg["qte"] = ml_utils.smart_round_to_int(subs_bagg["qte"])
    subs_bagg = subs_bagg[subs_bagg["qte"] != 0]
    subs_bagg["id"] = utils.format_id(
        subs_bagg["id_pdv"],
        subs_bagg["id_artc"],
        subs_bagg["date_destination"].dt.strftime("%Y%m%d"),
    )
    subs_bagg = subs_bagg[["id", "qte"]]
    name = "_".join([str(i) for i in date_origin_bagg])
    subs_bagg.to_csv(
        path.path_submission(name),
        compression="zip",
        index=False,
    )
