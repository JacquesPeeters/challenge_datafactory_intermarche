import pandas as pd

from src import constants, ml_utils
from src.utils import timer

cols_category = [["lb_vent_rayn"], ["lb_vent_faml"]]


def fe_main_dataset(sales):
    sales["date_dayofweek"] = sales["date"].dt.dayofweek
    sales["date_dayofyear"] = sales["date"].dt.dayofyear
    sales["date_year"] = sales["date"].dt.year.astype(str)
    # sales['date_month'] = sales['date'].dt.month
    sales["date_day"] = sales["date"].dt.day
    return sales


def _get_fe_sales(sales: pd.DataFrame) -> pd.DataFrame:
    """Returns various rolling FE (mean, median, quantile, std) of cols
    ["qte", "qte_lop1p"] at ["id_pdv", "id_artc"] granularity

    Args:
        sales (pd.DataFrame): [description]

    Returns:
        pd.DataFrame: [description]
    """
    fe_sales_historical = sales[["date", "id_pdv", "id_artc"]].copy()

    sales["date_dow"] = sales["date"].dt.dayofweek

    # Rolling mean
    cols_group = ["id_pdv", "id_artc"]
    grouped = sales.groupby(cols_group)
    group_name = "_&_".join(col.upper() for col in cols_group)
    list_window = [1, 3] + [7 * 2 ** i for i in range(0, 7)]
    cols = ["qte", "qte_lop1p"]
    for window in list_window:
        cols_rolling = [f"{group_name}_{col}_mean_{window}" for col in cols]
        fe_sales_historical[cols_rolling] = grouped[cols].transform(
            lambda x: x.rolling(window, min_periods=1).mean()
        )

        cols_rolling = [f"{group_name}_{col}_median_{window}" for col in cols]
        fe_sales_historical[cols_rolling] = grouped[cols].transform(
            lambda x: x.rolling(window, min_periods=1).median()
        )

        cols_rolling = [f"{group_name}_{col}_std_{window}" for col in cols]
        fe_sales_historical[cols_rolling] = grouped[cols].transform(
            lambda x: x.rolling(window, min_periods=1).std()
        )

        cols_rolling = [f"{group_name}_{col}_quantile_0.80_{window}" for col in cols]
        fe_sales_historical[cols_rolling] = grouped[cols].transform(
            lambda x: x.rolling(window, min_periods=1).quantile(0.8)
        )

    fe_sales_historical = fe_sales_historical.rename(columns={"date": "date_origin"})
    return fe_sales_historical


def fe_products_first_sale(sales: pd.DataFrame) -> pd.DataFrame:
    """
    Date of first sale for ["id_pdv", "id_artc"]
    """
    cols_group = ["id_pdv", "id_artc"]
    group_name = "_&_".join(col.upper() for col in cols_group)
    products_first_sale = (
        sales.groupby(cols_group)["date"]
        .min()
        .rename(f"{group_name}_date_min")
        .reset_index()
    )
    return products_first_sale


def get_fe_prix(prix: pd.DataFrame) -> pd.DataFrame:
    """Average price by quarter for ["id_pdv", "id_artc"]

    Args:
        prix (pd.DataFrame): [description]

    Returns:
        pd.DataFrame: [description]
    """
    prix_fe = prix[["id_pdv", "id_artc", "date_destination"]].copy()

    # Rolling mean
    cols_group = ["id_pdv", "id_artc"]
    grouped = prix.groupby(cols_group)
    group_name = "_&_".join(col.upper() for col in cols_group)
    list_window = [1]
    cols = ["prix"]
    for window in list_window:
        cols_rolling = [f"{group_name}_{col}_mean_{window}" for col in cols]
        prix_fe[cols_rolling] = grouped[cols].transform(
            lambda x: x.rolling(window, min_periods=1).mean()
        )

    prix_fe["date_destination"] = prix_fe["date_destination"] + pd.Timedelta(days=1)
    return prix_fe


@timer
def get_fe_sales(sales, is_parallel):
    if is_parallel:
        sales_historical = ml_utils.parallelize_function_dataframe(
            dataframe=sales,
            function=_get_fe_sales,
            partition_by=["id_artc"],
            partition_count=32 * 2,
            cpu_count=32,
        )
    else:
        sales_historical = _get_fe_sales(sales)
    return sales_historical


def _get_fe_sales_dow(sales: pd.DataFrame) -> pd.DataFrame:
    """Returns rolling mean and median of cols ["qte", "qte_lop1p"] at
    ["id_pdv", "id_artc", "date_dow"] granularity

    Args:
        sales (pd.DataFrame): [description]

    Returns:
        pd.DataFrame: [description]
    """
    sales["date_dow"] = sales["date"].dt.dayofweek
    fe_sales_dow = sales[["date", "date_dow", "id_pdv", "id_artc"]].copy()

    cols_group = ["id_pdv", "id_artc", "date_dow"]
    grouped = sales.groupby(cols_group)
    group_name = "_&_".join(col.upper() for col in cols_group)
    cols = ["qte", "qte_lop1p"]
    list_window = [2 ** i for i in range(0, 7)]
    for window in list_window:
        cols_rolling = [f"{group_name}_{col}_mean_{window}" for col in cols]
        fe_sales_dow[cols_rolling] = grouped[cols].transform(
            lambda x: x.rolling(window, min_periods=1).mean()
        )

        cols_rolling = [f"{group_name}_{col}_median_{window}" for col in cols]
        fe_sales_dow[cols_rolling] = grouped[cols].transform(
            lambda x: x.rolling(window, min_periods=1).median()
        )

    # We want to perform a rolling join on date_origin but based on date_destination_dow
    fe_sales_dow = fe_sales_dow.rename(
        columns={"date": "date_origin", "date_dow": "date_destination_dow"}
    )

    return fe_sales_dow


@timer
def get_fe_sales_dow(sales, is_parallel):
    if is_parallel:
        fe_sales_dow = ml_utils.parallelize_function_dataframe(
            dataframe=sales,
            function=_get_fe_sales_dow,
            partition_by=["id_artc"],
            partition_count=32 * 2,
            cpu_count=32,
        )
    else:
        fe_sales_dow = _get_fe_sales_dow(sales)
    return fe_sales_dow


def fe_evaluated(fe_sales: pd.DataFrame) -> pd.DataFrame:
    """
    Detect target rows which are not evaluated during the challenge.
    See rules of the challenge:
    'L'évaluation s'applique uniquement aux prédictions attendues, c.-à-d. pour
    lesquelles au moins une vente réelle (du produit par le point de vente en
    question) a bien été constatée le jour même ou dans les six jours précédents.
    Dans le cas contraire, la prédiction ne sera pas pénalisée et n'entrera pas dans
    le calcul du score.'
    """
    evaluated = fe_sales[
        ["date_origin", "id_pdv", "id_artc", "ID_PDV_&_ID_ARTC_qte_lop1p_mean_7"]
    ].rename(
        columns={
            "date_origin": "date_destination",
        }
    )
    # Rolling mean produces small rounding errors. Since we want to filter out 0 fix
    # this.
    evaluated["ID_PDV_&_ID_ARTC_qte_lop1p_mean_7"] = evaluated[
        "ID_PDV_&_ID_ARTC_qte_lop1p_mean_7"
    ].round(10)
    # If we do not have 7 consecutive 0, observation is evaluated therefore fed to
    # the algo.
    evaluated["evaluated"] = evaluated["ID_PDV_&_ID_ARTC_qte_lop1p_mean_7"] != 0

    evaluated = evaluated.drop(columns="ID_PDV_&_ID_ARTC_qte_lop1p_mean_7")

    return evaluated


def get_group_dow_trend(
    nomenclature_sales: pd.DataFrame, cols_group: list
) -> pd.DataFrame:
    """
    Compute day of the week (dow / weekly) sales  at granularity cols_group
    Final dataset granularity is cols_group + ['date_destination_dow'].

    Note: we are cheating a little bit here should be done out-of-fold, but we assume
    overfitting is small.

    Args:
        nomenclature_sales (pd.DataFrame): [description]
        cols_group (list): granularity of the target encoding

    Returns:
        pd.DataFrame: [description]
    """
    nomenclature_sales["date_dow"] = nomenclature_sales["date"].dt.dayofweek
    cols_group_dow = cols_group + ["date_dow"]
    group_name = "_&_".join([col.upper() for col in cols_group_dow])
    category_dow = nomenclature_sales.groupby(cols_group_dow)["qte"].sum().reset_index()

    category_dow[f"{group_name}_qte_pct"] = category_dow["qte"] / category_dow.groupby(
        cols_group
    )["qte"].transform("sum")

    category_dow = category_dow.rename(
        columns={"qte": f"{group_name}_qte", "date_dow": "date_destination_dow"}
    )
    return category_dow


def get_yearly_trend_week(
    nomenclature_sales: pd.DataFrame, cols_group: list
) -> pd.DataFrame:
    """Yearly saisonnality for weeks at cols_group granularity. To unbias the number of
    products active products, we multiply this seasonality by the inverse of the
    variation of active products.

    Note : This feature is leaked but consider this as our best estimate for yearly
    saisonnality. Should be done out-of-fold in prod.

    Args:
        nomenclature_sales (pd.DataFrame): [description]
        cols_group (list): [description]

    Returns:
        pd.DataFrame: [description]
    """
    group_name = "_&_".join(col.upper() for col in cols_group)

    nomenclature_sales_week = nomenclature_sales.copy()
    # Re-implement the week of year because 31 December considered as first week
    nomenclature_sales_week["date_week"] = (
        nomenclature_sales_week["date"].dt.dayofyear - 1
    ) // 7

    nomenclature_sales_week[nomenclature_sales_week["date_week"] == 1].date.unique()

    agg_func = {"qte": "sum", "id_artc": "nunique"}

    nomenclature_sales_week = (
        nomenclature_sales_week.groupby(cols_group + ["date_week"])
        .agg(agg_func)
        .reset_index()
    )

    # If we had multiple years we should do this merge differently
    yearly_trend_week = pd.merge(
        nomenclature_sales_week.rename(
            columns={
                "date_week": "date_origin_week",
                "qte": "qte_origin",
                "id_artc": "id_artc_origin",
            }
        ),
        nomenclature_sales_week.rename(
            columns={
                "date_week": "date_destination_week",
                "qte": "qte_destination",
                "id_artc": "id_artc_destination",
            }
        ),
    )

    cond = (
        (
            yearly_trend_week["date_destination_week"]
            - yearly_trend_week["date_origin_week"]
        )
        .mod(53)
        .between(0, (constants.HORIZON_MAX + 7) // 7)
    )

    # Remove impossible combination
    yearly_trend_week = yearly_trend_week[cond]
    yearly_trend_week[f"{group_name}_yearly_trend_week"] = (
        yearly_trend_week["qte_destination"] / yearly_trend_week["qte_origin"]
    )

    # If we had less product at the beginning we need to unbias by the inverse number
    # of active products variation
    yearly_trend_week["inverse_active_products"] = (
        yearly_trend_week["id_artc_origin"] / yearly_trend_week["id_artc_destination"]
    )

    yearly_trend_week[f"{group_name}_yearly_trend_week"] = yearly_trend_week[
        f"{group_name}_yearly_trend_week"
    ].round(2)

    yearly_trend_week[f"{group_name}_yearly_trend_week"] = (
        yearly_trend_week[f"{group_name}_yearly_trend_week"]
        * yearly_trend_week["inverse_active_products"]
    )

    yearly_trend_week = yearly_trend_week[
        cols_group
        + ["date_origin_week", "date_destination_week"]
        + [f"{group_name}_yearly_trend_week"]
    ]

    return yearly_trend_week


@timer
def get_learning(
    main_learning: pd.DataFrame,
    left_join: [pd.DataFrame],
    backward_join: [pd.DataFrame],
    backward_join_destination,
) -> pd.DataFrame:
    """Merge our different datasets altogether

    Args:
        main_learning (pd.DataFrame): main dataset
        left_join ([type]): list of featuresets to left-join
        backward_join ([type]): list of featuresets to backward-join (on date_origin)
        backward_join_destination ([type]): list of featuresets to backward-join on the
        date_destination
    Returns:
        pd.DataFrame: learning/prod set
    """
    learning = main_learning.copy()

    # date fe needed for later join maybe should be a common func with associated fe
    learning["date_destination_dow"] = learning["date_destination"].dt.dayofweek

    # needed for joining with get_yearly_trend_week()
    learning["date_origin_week"] = (learning["date_destination"].dt.dayofyear - 1) // 7
    learning["date_destination_week"] = (
        learning["date_destination"].dt.dayofyear - 1
    ) // 7

    # Pure fe maybe should be somewhere else
    learning["date_destination_dayofmonth"] = learning["date_destination"].dt.day

    nrow_begin = learning.shape[0]
    for featuresets in left_join:
        learning = pd.merge(learning, featuresets, how="left")
        assert nrow_begin == learning.shape[0]

    col_time = "date_origin"
    learning = learning.sort_values(col_time)
    for featuresets in backward_join:
        featuresets = featuresets.sort_values(col_time)
        cols_by = list(learning.columns.intersection(featuresets.columns))
        cols_by.remove(col_time)
        learning = pd.merge_asof(
            learning,
            featuresets,
            on=col_time,
            by=cols_by,
            allow_exact_matches=True,
        )
        assert nrow_begin == learning.shape[0]

    col_time = "date_destination"
    learning = learning.sort_values(col_time)
    for featuresets in backward_join_destination:
        featuresets = featuresets.sort_values(col_time)
        cols_by = list(learning.columns.intersection(featuresets.columns))
        cols_by.remove(col_time)
        learning = pd.merge_asof(
            learning,
            featuresets,
            on=col_time,
            by=cols_by,
            allow_exact_matches=True,
        )
        assert nrow_begin == learning.shape[0]

    return learning


def fe_learning(learning):
    """Contextual feature engineering like price trend, age of the products
    https://medium.com/manomano-tech/a-framework-for-feature-engineering-and-machine-learning-pipelines-ddb53867a420 :) # noqa
    """
    # Price variation
    learning["prix_/_ID_PDV_&_ID_ARTC_prix_mean_1"] = (
        learning["prix"] / learning["ID_PDV_&_ID_ARTC_prix_mean_1"]
    )

    # Unbias our FE by providing the product age
    learning["ID_PDV_&_ID_ARTC_date_origin_MINUS_date_min"] = (
        learning["date_origin"] - learning["ID_PDV_&_ID_ARTC_date_min"]
    ).dt.days

    return learning
