import numpy as np
from sklearn.metrics import mean_squared_error, mean_squared_log_error


def test_mean_sqared_log_error():
    pred = [0, 1, 2, 3, 4]
    target = [0, 1.2, 2, 4, 3]
    res_mean_squared_error = mean_squared_error(target, pred)
    res_mean_squared_log_error = mean_squared_log_error(
        y_true=np.expm1(target),
        y_pred=np.expm1(pred),
    )
    assert res_mean_squared_error == res_mean_squared_log_error
