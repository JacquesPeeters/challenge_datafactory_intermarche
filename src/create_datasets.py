# import os
import ipdb  # noqa - fix relative imports problems

# How to make port redirection from DS1 to local to access MLflow
# ssh -N -f -L localhost:5000:localhost:5000 jpeeters@ds1
# (replace jpeeters with your username)
import pandas as pd

from src import constants
from src import feature_engineering as fe
from src import path
from src import preprocessing as prep


def create_datasets():
    """Create learning (train/validation) and production dataset. To do so various
    featuresets are created then merged altogether."""
    print(f"CURRENT_ENVIRONMENT: {constants.CURRENT_ENVIRONMENT}")
    sales = prep.read_sales()
    prix = prep.read_prix()
    nomenclature = prep.read_nomenclature()

    nomenclature_sales = pd.merge(sales, nomenclature)

    holidays_fr = prep.get_holidays_fr()

    category_dow_trend = []
    cols_group_dow = (
        fe.cols_category + [["id_pdv"]] + [col + ["id_pdv"] for col in fe.cols_category]
    )

    for cols_group in cols_group_dow:
        tmp_category_dow_trend = fe.get_group_dow_trend(
            nomenclature_sales, cols_group=cols_group
        )
        category_dow_trend.append(tmp_category_dow_trend)

    cols_group_yearly_trend_week = [
        ["lb_vent_rayn"],
        # ["id_pdv"], => Should have added this but forgot
    ]

    l_yearly_trend_week = []
    for cols_group in cols_group_yearly_trend_week:
        l_yearly_trend_week.append(
            fe.get_yearly_trend_week(nomenclature_sales, cols_group)
        )

    sales = prep.sample_dataset(sales)
    sales = prep.complete_sales(sales)

    products_first_sale = fe.fe_products_first_sale(sales)

    fe_prix = fe.get_fe_prix(prix)

    fe_sales = fe.get_fe_sales(sales, is_parallel=constants.IS_PARALLEL)
    fe_sales_dow = fe.get_fe_sales_dow(sales, is_parallel=constants.IS_PARALLEL)

    evaluated = fe.fe_evaluated(fe_sales)

    main_learning = prep.generate_main_learning(sales)

    left_join = [
        fe_sales,
        products_first_sale,
        nomenclature,
        holidays_fr,
    ] + category_dow_trend
    backward_join = [fe_sales_dow]
    backward_join_destination = [prix, fe_prix, evaluated]

    learning = fe.get_learning(
        main_learning,
        left_join,
        backward_join,
        backward_join_destination,
    )
    learning = fe.fe_learning(learning)

    learning.to_parquet(path.path_learning)

    if constants.CURRENT_ENVIRONMENT == "prod":
        del learning  # Save memory space

        for date_origin in constants.RANGE_DATE_ORIGIN_PROD:
            print(date_origin)
            main_prod = prep.generate_main_prod(sales, date_origin)
            prod = fe.get_learning(
                main_prod,
                left_join,
                backward_join,
                backward_join_destination,
            )
            prod = fe.fe_learning(prod)
            prod.to_parquet(path.path_prod(date_origin))
