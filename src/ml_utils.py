import multiprocessing
from functools import partial
from multiprocessing import Pool

import numpy as np
import pandas as pd
import plotnine as pn
from mizani.formatters import percent_format
from plotnine import aes, ggplot
from sklearn.metrics import mean_squared_error

plotnine_coef = 1.5
pn.options.figure_size = (6.4 * plotnine_coef, 4.8 * plotnine_coef)


def parallelize_function_dataframe(
    dataframe: pd.DataFrame,
    function,
    partition_by: list = None,
    partition_count: int = None,
    cpu_count: int = None,
    **kwargs,
) -> pd.DataFrame:
    """Apply a function to a dataframe in a parallelized way.
    Best practice : It takes time to copy chunk of dataframe to each core. So a best
    practice is to feed this function with only the columns needed. It also reduces
    memory peak.

    Args:
        dataframe (pd.DataFrame): [description]
        function ([type]): [description]
        partition_by (list, optional): If our function use groupby, groups should be in
        the same partition. Defaults to None.
        partition_count (int, optional): [description]. Defaults to None.
        cpu_count (int, optional): [description]. Defaults to None.

    Returns:
        pd.DataFrame: [description]
    """
    if partition_by is None:
        df_split = np.array_split(dataframe, partition_count)
    else:
        # Not very effective to split then concat back
        # TODO improve this
        chunk = dataframe[partition_by].drop_duplicates()
        chunk["no_other_variable_name"] = 1
        chunk["no_other_variable_name"] = (
            chunk["no_other_variable_name"].cumsum().mod(partition_count)
        )
        dataframe = pd.merge(dataframe, chunk, how="left")
        df_split = [
            dataframe[dataframe["no_other_variable_name"] == n]
            for n in chunk["no_other_variable_name"].unique()
        ]

    if multiprocessing.cpu_count() > cpu_count:
        print(f"Warning: {multiprocessing.cpu_count()} > {cpu_count}")

    pool = Pool(partition_count)
    dataframe = pd.concat(pool.map(partial(function, **kwargs), df_split))
    pool.close()
    pool.join()

    dataframe = dataframe.drop(columns="no_other_variable_name", errors="ignore")

    return dataframe


def get_importance_lgb(model_gbm, X_cols=None):
    importance = pd.DataFrame()
    if X_cols is None:
        importance["feature"] = model_gbm.feature_name()
    else:
        importance["feature"] = X_cols
    importance["importance"] = model_gbm.feature_importance(importance_type="gain")
    importance["importance"] = (
        importance["importance"] / importance["importance"].replace(np.inf, 0).sum()
    )
    importance["importance"] = importance["importance"] * 100
    importance["importance_rank"] = importance["importance"].rank(
        ascending=False
    )  # .astype(int)
    importance = importance.sort_values("importance_rank").round(2)
    return importance


def plot_importance_lgb(importance):
    importance = importance.copy()  # Do not erase the given dataset
    importance["importance"] = importance["importance"] / 100
    importance["feature"] = pd.Categorical(
        importance["feature"], importance["feature"][::-1], ordered=True
    )
    plot = (
        ggplot(importance, aes("feature", "importance"))
        + pn.geom_bar(stat="identity")
        + pn.coords.coord_flip()
        + pn.scales.scale_y_continuous(labels=percent_format())
        + pn.labs(title="Feature importance", x="Feature", y="Gain")
    )
    return plot


def _smart_round_to_int(x_float: float) -> int:
    """Round smarly to the lower of upper integer in order to minimize rmlse

    Args:
        x_float (float): [description]

    Returns:
        int:
    """
    x_float_log1p = np.log1p(x_float)
    lower = np.log1p(np.floor(x_float))
    upper = np.log1p(np.ceil(x_float))

    loss_lower = mean_squared_error([lower], [x_float_log1p])
    loss_upper = mean_squared_error([upper], [x_float_log1p])

    if loss_lower < loss_upper:
        return np.floor(x_float)
    else:
        return np.ceil(x_float)


_smart_round_to_int_v = np.vectorize(_smart_round_to_int)


def smart_round_to_int(pred: pd.Series) -> pd.Series:
    # see _smart_round_to_int documentation
    pred_rounded = pred.round(2)
    uniques = pred_rounded.unique()
    mapping_round_to_int = dict(zip(uniques, _smart_round_to_int_v(uniques)))
    return pred_rounded.map(mapping_round_to_int).astype(int)
