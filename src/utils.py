import functools
import time

import numpy as np


def timer(func):
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        print(func.__name__)
        tic = time.perf_counter()
        value = func(*args, **kwargs)
        toc = time.perf_counter()
        elapsed_time = toc - tic
        print(f"Elapsed time: {elapsed_time:0.1f} seconds")
        return value

    return wrapper_timer


def format_id(id_pdv, id_artc, date_destination):
    return f"{id_pdv}_{id_artc}_{date_destination}"


format_id = np.vectorize(format_id)
