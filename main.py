from src import create_datasets, train_predict


def run_pipeline() -> None:
    """Run Customer pipeline (equivalent to an Airflow DAG)"""
    create_datasets.create_datasets()
    train_predict.train()
    train_predict.predict()
    train_predict.bag_sub_date_origin()


if __name__ == "__main__":
    # DISCLAIMER : PLace yourself at the root of the project to make this code work
    # export CURRENT_ENVIRONMENT='my_desired_env' # Default to the one defined in
    # constants.py
    # python main.py
    run_pipeline()
